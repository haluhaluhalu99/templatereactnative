import React, { FC } from 'react';
import { StyleSheet, TouchableNativeFeedback, ViewStyle, TextStyle } from 'react-native';
import { Button, ButtonProps } from 'react-native-elements';
import { color, fontSize, padding,isIos  } from '@helpers/index';

export interface BaseButtonProps extends ButtonProps {
  titleStyle?: TextStyle | any;
  buttonStyle?: ViewStyle;
  containerStyle?: ViewStyle;
  disabled?: boolean;
}

const AppButton: FC<BaseButtonProps> = React.memo((props) => {
  const { titleStyle, buttonStyle, containerStyle, disabled, ...restProps } = props;
  return (
    <Button
      containerStyle={StyleSheet.flatten([styles.container, containerStyle])}
      titleStyle={StyleSheet.flatten([styles.title, titleStyle])}
      buttonStyle={StyleSheet.flatten([
        styles.button,
        buttonStyle,
        {
          paddingHorizontal: props?.icon ? padding.p12 : padding.p24,
        },
      ])}
      background={TouchableNativeFeedback.Ripple(color.gray, false)}
      activeOpacity={0.6}
      disabled={disabled}
      {...restProps}
    />
  );
});

const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    marginBottom: padding.p8,
  },
  button: {
    backgroundColor: color.primary,
    borderWidth: 0,
    paddingVertical: padding.p12,
  },
  title: {
    fontSize: fontSize.f16,
    color: color.white,
    fontWeight: 'bold',
  },
});

export default AppButton;
