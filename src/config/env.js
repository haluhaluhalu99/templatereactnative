import { STAGING_BASE_URL, DEV_BASE_URL } from '@env';

const devEnvironmentVariables = {
  BACKEND_URL: DEV_BASE_URL,
};

const stagEnvironmentVariables = {
  BACKEND_URL: STAGING_BASE_URL,
};

export default __DEV__ ? devEnvironmentVariables : stagEnvironmentVariables;
